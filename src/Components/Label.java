package Components;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Label extends Common{
	protected static WebDriver driver;
	static Logger log = Logger.getLogger(Label.class);
	public Label(WebDriver driver) {
		Label.driver = driver;
	}

	public int getListCount(String label) {
		int count =0;
		try {
			String xpath = "//div[@class='"+label+"']|//div[contains(@id,'"+label+"')]";
			waitForElementToAppear(driver,By.xpath(xpath));
			count = driver.findElements(By.xpath(xpath)).size();
		}catch(Exception e) {
			throw e;
		}

		return count;

	}
	
	public boolean isLabelDisplayed(String label) {
		boolean elementfound = false;
		try {
			String xpath = "//div[@class='"+label+"']|//div[contains(@id,'"+label+"')]";
			waitForElementToAppear(driver,By.xpath(xpath));
			elementfound = isDisplayed(driver,By.xpath(xpath));
		}catch(Exception e) {
			throw e;
		}
return elementfound;
	}
}
