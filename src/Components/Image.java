package Components;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Image extends Common{
	protected static WebDriver driver;
	static Logger log = Logger.getLogger(Image.class);
	public Image(WebDriver driver) {
		Image.driver = driver;
	}
	public int getListCount(String label) {
		int count =0;
		try {
			String xpath = "//img[@alt='"+label+"']";
			waitForElementToAppear(driver,By.xpath(xpath));
			count = driver.findElements(By.xpath(xpath)).size();
		}catch(Exception e) {
			throw e;
		}

		return count;

	}
	public void clickImage(String label) {
		try {
			String xpath = "//img[@alt='"+label+"']";
			waitForElementToAppear(driver,By.xpath(xpath));
			Click(driver,By.xpath(xpath));
			log.info("The image "+ label +"has been clicked");
		}catch(Exception e) {
			log.error("The image "+ label +"has not been clicked  due to "+e.getMessage());
			throw e;
		}
	}
}
