package Components;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



public class TextField extends Common{
	protected static WebDriver driver;
	static Logger log = Logger.getLogger(TextField.class);
	public TextField(WebDriver driver) {
		TextField.driver = driver;
	}

	public void setTextFieldValue(String label,String value) {
		try {
			String xpath = "//input[@placeholder='"+label+"']";
			waitForElementToAppear(driver,By.xpath(xpath));
			setValue(driver,By.xpath(xpath),value);
			log.info("The Value "+value+"has been set in the text field "+label);
		}catch(Exception e) {
			log.error("The Value "+value+"has been set in the text field "+label+" due to "+e.getMessage());
			throw e;
		}
	}

}
