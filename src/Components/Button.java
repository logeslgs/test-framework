package Components;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Button extends Common{
	protected static WebDriver driver;
	static Logger log = Logger.getLogger(Button.class);
	public Button(WebDriver driver) {
		Button.driver = driver;

	}
	
	public void clickButton(String label) {
		try {
			String xpath = "//button[text()='"+label+"']";
			waitForElementToAppear(driver,By.xpath(xpath));
			Click(driver,By.xpath(xpath));
			log.info("The button "+ label +"has been clicked");
		}catch(Exception e) {
			log.error("The button "+ label +"has not been clicked  due to "+e.getMessage());
			throw e;
		}
	}

}
