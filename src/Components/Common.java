package Components;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Panels.CommonUtils;

public class Common {

	private static final int TIMEOUT = 60;
	private static final int POLLING = 500;
	static Logger log = Logger.getLogger(Common.class);
	

	public void waitForElementToAppear(WebDriver driver,By locator) {
		try {
		WebDriverWait wait = new WebDriverWait(driver, TIMEOUT, POLLING);
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		
		}catch(Exception e) {
			
			throw e;
		}
	}
	
	public void setValue(WebDriver driver,By locator,String value) {
		try {
		driver.findElement(locator).sendKeys(value);
		
		}catch(Exception e) {
			
			throw e;
		}
	}
	public void Click(WebDriver driver,By locator) {
		try {
		driver.findElement(locator).click();
		
		}catch(Exception e) {
			
			throw e;
		}
	}
	
	public boolean isDisplayed(WebDriver driver,By locator) {
		boolean elementfound = false;
		try {
			if(driver.findElement(locator).isDisplayed()) {
				elementfound = true;
			}
			
			}catch(Exception e) {
				
				throw e;
			}
		return elementfound;
		
	}

}
