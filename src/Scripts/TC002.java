package Scripts;

import Panels.CommonUtils;
import Panels.ProductListPage;
import Panels.ProductPage;

public class TC002 {

	public static void main(String[] args) {
		CommonUtils commonutils = new CommonUtils();
		int lastrownum = commonutils.getSheet(System.getProperty("user.dir")+"\\Data.xlsx", TC002.class.getSimpleName()).getLastRowNum();
		int rownum = 1;
		do {
		commonutils.launchBrowser(commonutils.readUserData( "BrowserType", rownum));
		ProductPage productpage = new ProductPage();
		commonutils.navigateToURL(commonutils.readUserData( "URL", rownum));
		productpage.verifyExperienceDisplayed();
		int recommendationcount = productpage.getRecommendationCount();
		commonutils.verifyCondition(recommendationcount > 0);
		commonutils.closeBrowser();
		rownum++;
		}while(rownum <= lastrownum);


	}

}
