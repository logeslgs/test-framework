package Scripts;

import Panels.CommonUtils;
import Panels.ProductListPage;

public class TC001 {


	public static void main(String[] args) {
		CommonUtils commonutils = new CommonUtils();
		int lastrownum = commonutils.getSheet(System.getProperty("user.dir")+"\\Data.xlsx", TC001.class.getSimpleName()).getLastRowNum();
		int rownum = 1;
		do {
		commonutils.launchBrowser(commonutils.readUserData( "BrowserType", rownum));
		ProductListPage productlistpage = new ProductListPage();
		commonutils.navigateToURL(commonutils.readUserData( "URL", rownum));
		int productcount = productlistpage.getProductCount();
		int experiencecount = productlistpage.getExperienceImageCount();
		commonutils.verifyCondition(productcount==experiencecount);
		productlistpage.clickExperience();
		productlistpage.verifyExperienceDisplayed();
		commonutils.closeBrowser();
		rownum++;
		}while(rownum <= lastrownum);

	}

}
