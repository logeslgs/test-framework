package Scripts;

import Panels.CommonUtils;
import Panels.Dashboard;

public class TC003 {

	public static void main(String[] args) {
		CommonUtils commonutils = new CommonUtils();
		int lastrownum = commonutils.getSheet(System.getProperty("user.dir")+"\\Data.xlsx", TC003.class.getSimpleName()).getLastRowNum();
		int rownum = 1;
		do {
			commonutils.launchBrowser(commonutils.readUserData( "BrowserType", rownum));
			Dashboard dashboard = new Dashboard();
			commonutils.navigateToURL(commonutils.readUserData( "URL", rownum));
			dashboard.setLoginUserName(commonutils.readUserData( "UserName", rownum));
			dashboard.setLoginPassword(commonutils.readUserData( "Password", rownum));
			dashboard.clickLogin();
			//commonutils.closeBrowser();
			rownum++;
		}while(rownum <= lastrownum);

	}
}


