package Panels;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Components.Button;
import Components.TextField;

public class Dashboard extends CommonUtils{
	@SuppressWarnings("static-access")
	TextField textfield = new TextField(drv);
	Button button = new Button(drv);
	static Logger log = Logger.getLogger(Dashboard.class);
	
	public void setLoginUserName(String value) {
		try {
			textfield.setTextFieldValue("Work E-mail", value);
			log.info("The User name has been set.");
		}catch(Exception e) {
			log.error("The User name has not been set due to "+e.getMessage());
		}
		
	}
	public void setLoginPassword(String value) {
		try {
			textfield.setTextFieldValue("Password", value);
			log.info("The Password has been set.");
		}catch(Exception e) {
			log.error("The Password has not been set due to "+e.getMessage());
		}
		
	}
	public void clickLogin() {
		try {
			button.clickButton("Log In");
			log.info("Log In has been clicked");
		}catch(Exception e) {
			log.error("Log In has not been clicked "+e.getMessage());
		}
		
	}
}
