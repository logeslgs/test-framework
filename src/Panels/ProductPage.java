package Panels;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Components.Image;
import Components.Label;

public class ProductPage extends CommonUtils{
	Label label = new Label(drv);
	Image image = new Image(drv);
		
		static Logger log = Logger.getLogger(ProductListPage.class);
	
		public void verifyExperienceDisplayed() {
			try {
				if(label.isLabelDisplayed("vue-item-wrapper vue-slider-item-wrapper")) {
					log.info("The Experience is loaded.");
				}else {
					log.error("The Experience is not loaded.");
				}
				
			}catch(Exception e) {
				log.error("Error occurred due to "+e.getMessage());
			}
		}
		public int getRecommendationCount() {
			int count = 0;
			try {
				count = label.getListCount("vue-item-wrapper vue-slider-item-wrapper");
				log.info("The number of products displayed is "+ count);
			}catch(Exception e) {
				log.error("Error occurred due to "+e.getMessage());
			}
			return count;
		}
}
