package Panels;


import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class CommonUtils {
	public static WebDriver drv = null ;
	public static Sheet sheet = null;
	static Logger log = Logger.getLogger(CommonUtils.class);


	public WebDriver launchBrowser(String browsertype) {
		PropertyConfigurator.configure("log4j.properties");
		try {
			switch(browsertype.toLowerCase()) {
			case "firefox":
				System.setProperty("webdriver.firefox.marionette",System.getProperty("user.dir")+"\\geckodriver.exe");
				drv = new FirefoxDriver();
				break;

			case "chrome":
				System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\chromedriver.exe");
				drv = new ChromeDriver();
				break;

			case "ie":
				System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\IEDriverServer.exe");
				drv = new InternetExplorerDriver();
				break;
			}
			log.info("The browser "+browsertype+" has been launched.");
		}catch(Exception e) {
			log.error("The browser "+browsertype+" has not been launched due to "+e.getMessage());	
		}

		return drv;
	}

	public void navigateToURL(String URL) {
		try {
			drv.get(URL);
			log.info("Navigation to "+URL+"is completed");
		}catch(Exception e) {
			log.error("Navigation to "+URL+"is not completed due to "+e.getMessage());	
		}
	}
	
	 public Sheet getSheet(String workbookname, String sheetname) {
		 try {
			 File file =    new File(workbookname);
			 FileInputStream inputStream = new FileInputStream(file);
			 Workbook workbook = WorkbookFactory.create(inputStream);
			 sheet = workbook.getSheet(sheetname);	 
			 workbook.close();
			 inputStream.close();
		 }catch(Exception e){
			 
		 }
		 return sheet; 
	 }
	 public String readUserData(String columnname, int rownum) {
		 String value ="";
		 try {
			 Row row = sheet.getRow(0);
			 int columnindex = 0;
			 for(Cell cell: row) {
				 if(cell.getStringCellValue().equalsIgnoreCase(columnname)) {
					 columnindex = cell.getColumnIndex();
					 break;
				 }
			 }
			 value = sheet.getRow(rownum).getCell(columnindex).getStringCellValue();
			 
		 }catch(Exception e) {
			 
		 }
		 return value;
	 }
	 public void closeBrowser() {
		 try {
			 drv.close();
			 log.info("The browser has been closed.");
		 }catch(Exception e) {
			 log.info("The browser has not been closed due to "+e.getMessage());
		 }
	 }
	 public void verifyCondition(boolean condition) {
		 if(condition) {
			 log.info("The condition is satisfied");
		 }else {
			log.error("The condition is not satisfied"); 
		 }
	 }
}
