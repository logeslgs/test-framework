package Panels;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import Components.Image;
import Components.Label;


public class ProductListPage extends CommonUtils{
Label label = new Label(drv);
Image image = new Image(drv);
	
	static Logger log = Logger.getLogger(ProductListPage.class);
	
	public int getProductCount() {
		int count = 0;
		try {
			count = label.getListCount("ProductCardImageWrapper");
			log.info("The number of products displayed is "+ count);
		}catch(Exception e) {
			log.error("Error occurred due to "+e.getMessage());
		}
		return count;
	}
	public int getExperienceImageCount() {
		int count = 0;
		try {
			count = image.getListCount("Style it with");
			log.info("The number of images displayed is "+ count);
		}catch(Exception e) {
			log.error("Error occurred due to "+e.getMessage());
		}
		return count;
	}
	
	public void verifyExperienceDisplayed() {
		try {
			if(label.isLabelDisplayed("vue-carousel-slide-item-wrapper")) {
				log.info("The Experience is loaded.");
			}else {
				log.error("The Experience is not loaded.");
			}
			
		}catch(Exception e) {
			log.error("Error occurred due to "+e.getMessage());
		}
	}
	public void clickExperience() {
		try {
			image.clickImage("Style it with");
			log.info("The Image has been clicked");
		}catch(Exception e) {
			log.error("The Image has not  been clicked due to "+e.getMessage());
		}
	}
}
